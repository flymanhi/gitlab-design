window.__imported__ = window.__imported__ || {};
window.__imported__["27724-large-build-logs@2x/layers.json.js"] = [
	{
		"objectId": "F9522E16-769A-416F-B1AF-CEA7269DA8F5",
		"kind": "artboard",
		"name": "prototype",
		"originalName": "prototype",
		"maskFrame": null,
		"layerFrame": {
			"x": -53,
			"y": 310,
			"width": 988,
			"height": 574
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "7EB09698-3581-48FD-9E0C-E7CC91A2A732",
				"kind": "group",
				"name": "scrollcontent",
				"originalName": "scrollcontent",
				"maskFrame": null,
				"layerFrame": {
					"x": 31,
					"y": -37,
					"width": 771,
					"height": 880
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-scrollcontent-n0vcmdk2.jpg",
					"frame": {
						"x": 31,
						"y": -37,
						"width": 771,
						"height": 880
					}
				},
				"children": []
			},
			{
				"objectId": "5915CB6D-2258-4624-AF80-DFFCBB68131F",
				"kind": "group",
				"name": "environment_info",
				"originalName": "environment-info",
				"maskFrame": null,
				"layerFrame": {
					"x": 15,
					"y": -24,
					"width": 960,
					"height": 52
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-environment_info-ntkxnunc.png",
					"frame": {
						"x": 15,
						"y": -24,
						"width": 960,
						"height": 52
					}
				},
				"children": [
					{
						"objectId": "B9B4D16C-53D5-4C28-A99F-71F437B41188",
						"kind": "group",
						"name": "Parent_Section",
						"originalName": "Parent Section",
						"maskFrame": null,
						"layerFrame": {
							"x": 31,
							"y": -9,
							"width": 381,
							"height": 22
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "2717A7B1-B276-49A8-A3F6-196C6914A6A2",
								"kind": "group",
								"name": "Group",
								"originalName": "Group",
								"maskFrame": null,
								"layerFrame": {
									"x": 31,
									"y": -9,
									"width": 381,
									"height": 22
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "9FC1BD37-8373-41B0-8226-8A555CCB30B2",
										"kind": "group",
										"name": "Group_9",
										"originalName": "Group 9",
										"maskFrame": null,
										"layerFrame": {
											"x": 31,
											"y": -9,
											"width": 381,
											"height": 22
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9-ouzdmuje.png",
											"frame": {
												"x": 31,
												"y": -9,
												"width": 381,
												"height": 22
											}
										},
										"children": []
									}
								]
							}
						]
					}
				]
			},
			{
				"objectId": "8CC4D230-86E1-4E99-942A-4933451E9DC1",
				"kind": "group",
				"name": "scroll_bar",
				"originalName": "scroll-bar",
				"maskFrame": null,
				"layerFrame": {
					"x": 957,
					"y": 86,
					"width": 4,
					"height": 161
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-scroll_bar-oendneqy.png",
					"frame": {
						"x": 957,
						"y": 86,
						"width": 4,
						"height": 161
					}
				},
				"children": []
			},
			{
				"objectId": "CADEB6AE-169C-48AD-9BF5-AC940B308AF2",
				"kind": "text",
				"name": "button_more",
				"originalName": "button_more",
				"maskFrame": null,
				"layerFrame": {
					"x": 957,
					"y": 51,
					"width": 4,
					"height": 15
				},
				"visible": false,
				"metadata": {
					"opacity": 1,
					"string": "",
					"css": [
						"/* : */",
						"font-family: FontAwesome;",
						"font-size: 15px;",
						"color: #FFFFFF;",
						"letter-spacing: 0;"
					]
				},
				"image": {
					"path": "images/Layer-button_more-q0ferui2.png",
					"frame": {
						"x": 957,
						"y": 51,
						"width": 4,
						"height": 15
					}
				},
				"children": []
			},
			{
				"objectId": "2D7FF2ED-3A2A-41B0-B2E1-BC00D5E68A99",
				"kind": "group",
				"name": "button_up",
				"originalName": "button_up",
				"maskFrame": null,
				"layerFrame": {
					"x": 952,
					"y": 50,
					"width": 12,
					"height": 16
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-button_up-mkq3rkyy.png",
					"frame": {
						"x": 952,
						"y": 50,
						"width": 12,
						"height": 16
					}
				},
				"children": [
					{
						"objectId": "746A021B-F52E-437F-B35F-835466F6D666",
						"kind": "group",
						"name": "Group_2_Copy_2",
						"originalName": "Group 2 Copy 2",
						"maskFrame": null,
						"layerFrame": {
							"x": 952,
							"y": 86,
							"width": 12,
							"height": 14
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2_Copy_2-nzq2qtay.png",
							"frame": {
								"x": 952,
								"y": 86,
								"width": 12,
								"height": 14
							}
						},
						"children": []
					},
					{
						"objectId": "6D8AB523-168D-43AF-8B36-2817529D4102",
						"kind": "group",
						"name": "Group_2_Copy",
						"originalName": "Group 2 Copy",
						"maskFrame": null,
						"layerFrame": {
							"x": 952,
							"y": 49,
							"width": 12,
							"height": 16
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2_Copy-nkq4qui1.png",
							"frame": {
								"x": 952,
								"y": 49,
								"width": 12,
								"height": 16
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "C35D2326-7FD7-4A18-968E-3E311A8BEF54",
				"kind": "group",
				"name": "button_showlog",
				"originalName": "button_showlog",
				"maskFrame": null,
				"layerFrame": {
					"x": 847,
					"y": 51,
					"width": 12,
					"height": 14
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-button_showlog-qzm1rdiz.png",
					"frame": {
						"x": 847,
						"y": 51,
						"width": 12,
						"height": 14
					}
				},
				"children": []
			},
			{
				"objectId": "260E9E15-51D7-4E21-88B0-44A355E68181",
				"kind": "group",
				"name": "button_delete",
				"originalName": "button_delete",
				"maskFrame": null,
				"layerFrame": {
					"x": 917,
					"y": 51,
					"width": 12,
					"height": 14
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-button_delete-mjywrtlf.png",
					"frame": {
						"x": 917,
						"y": 51,
						"width": 12,
						"height": 14
					}
				},
				"children": []
			},
			{
				"objectId": "564935B5-C262-42AA-B0CB-78390A98E6BA",
				"kind": "group",
				"name": "button_download",
				"originalName": "button_download",
				"maskFrame": null,
				"layerFrame": {
					"x": 881,
					"y": 50,
					"width": 14,
					"height": 13
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-button_download-nty0otm1.png",
					"frame": {
						"x": 881,
						"y": 50,
						"width": 14,
						"height": 13
					}
				},
				"children": []
			},
			{
				"objectId": "CA9DF97C-727F-4494-B37F-C5727F5B861B",
				"kind": "group",
				"name": "button_down",
				"originalName": "button_down",
				"maskFrame": null,
				"layerFrame": {
					"x": 952,
					"y": 532,
					"width": 12,
					"height": 16
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-button_down-q0e5rey5.png",
					"frame": {
						"x": 952,
						"y": 532,
						"width": 12,
						"height": 16
					}
				},
				"children": [
					{
						"objectId": "E5FB734F-C7FF-4FF2-8CE7-15EAE0B7CDF6",
						"kind": "group",
						"name": "Group_2_Copy_21",
						"originalName": "Group 2 Copy 2",
						"maskFrame": null,
						"layerFrame": {
							"x": 952,
							"y": 499,
							"width": 12,
							"height": 14
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2_Copy_2-rtvgqjcz.png",
							"frame": {
								"x": 952,
								"y": 499,
								"width": 12,
								"height": 14
							}
						},
						"children": []
					},
					{
						"objectId": "894CD7DB-4D9C-4A97-92C0-5187A584709D",
						"kind": "group",
						"name": "Group_2_Copy1",
						"originalName": "Group 2 Copy",
						"maskFrame": null,
						"layerFrame": {
							"x": 952,
							"y": 532,
							"width": 12,
							"height": 16
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group_2_Copy-odk0q0q3.png",
							"frame": {
								"x": 952,
								"y": 532,
								"width": 12,
								"height": 16
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "65B92C47-69E4-4ECE-8147-D3B904120C2C",
				"kind": "group",
				"name": "log",
				"originalName": "log",
				"maskFrame": null,
				"layerFrame": {
					"x": 15,
					"y": 39,
					"width": 961,
					"height": 520
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-log-njvcotjd.png",
					"frame": {
						"x": 15,
						"y": 39,
						"width": 961,
						"height": 520
					}
				},
				"children": [
					{
						"objectId": "6D684932-90A1-41E1-A0C4-293D1DADA6C2",
						"kind": "group",
						"name": "Rectangle_14",
						"originalName": "Rectangle 14",
						"maskFrame": {
							"x": 0,
							"y": 0,
							"width": 959,
							"height": 517.822445561139
						},
						"layerFrame": {
							"x": 15,
							"y": 39,
							"width": 961,
							"height": 520
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Rectangle_14-nkq2odq5.png",
							"frame": {
								"x": 15,
								"y": 39,
								"width": 961,
								"height": 520
							}
						},
						"children": [
							{
								"objectId": "3640503F-A5B7-46C1-B293-E992EFFCB01A",
								"kind": "group",
								"name": "Group_12",
								"originalName": "Group 12",
								"maskFrame": null,
								"layerFrame": {
									"x": 349,
									"y": 47,
									"width": 259,
									"height": 20
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "F20DC0A0-FC4F-4945-BC10-C7240B331655",
										"kind": "group",
										"name": "Group_13",
										"originalName": "Group 13",
										"maskFrame": null,
										"layerFrame": {
											"x": 349,
											"y": 47,
											"width": 259,
											"height": 20
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_13-rjiwremw.png",
											"frame": {
												"x": 349,
												"y": 47,
												"width": 259,
												"height": 20
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "96734E6F-22E4-451B-81B9-FAB61CE508BB",
						"kind": "group",
						"name": "Merge_Request_Section",
						"originalName": "Merge Request Section",
						"maskFrame": null,
						"layerFrame": {
							"x": 32,
							"y": 38,
							"width": 590,
							"height": 152
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "A5C3B363-BE10-476C-9637-956E4D9597EA",
								"kind": "group",
								"name": "Row_Copy_2",
								"originalName": "Row Copy 2",
								"maskFrame": null,
								"layerFrame": {
									"x": 32.05020920502102,
									"y": 42,
									"width": 585,
									"height": 144
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-Row_Copy_2-qtvdm0iz.png",
									"frame": {
										"x": 32.05020920502102,
										"y": 42,
										"width": 585,
										"height": 144
									}
								},
								"children": []
							}
						]
					}
				]
			}
		]
	}
]