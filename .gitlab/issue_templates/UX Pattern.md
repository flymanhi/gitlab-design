### Problem

(What’s the problem that this pattern solves? Why is it worth solving?)

### Solution

(What’s the solution? Why is it like that? What are the benefits?)

### Example(s)

(One or more images showing the UX pattern. They don’t have to be in GitLab.)

### Usage

(When do you use this pattern? And how?)

#### Dos and dont's

(Use this table to add images and text describing what’s ok and not ok.)

| :white_check_mark:  Do | :stop_sign: Don’t |
|------------------------|-------------------|
|  |  |

### Related patterns

(List any related or similar solutions. If none, write: No related patterns)

### Links / references

### Pattern checklist

Make sure these are completed before closing the issue,
with a link to the relevant commit, if applicable.

1. [ ] Read our [contribution guidelines](/CONTRIBUTING.md), especially the [For GitLabbers](/CONTRIBUTING.md#for-gitlabbers-) section.
1. [ ] Create a Sketch file in your progress folder just for this pattern and make sure that:
   1. [ ] You have not created a duplicate of an existing pattern, symbol, layer style, or text style.
   1. [ ] You have used the proper method for creating the pattern depending on the complexity. Atoms and molecules are symbols, organisms are groups.
   1. [ ] Typography uses text styles. When applicable, colors use shared styles.
1. [ ] Ask a UXer to review your Sketch file, linking them to your latest commit so they know where to find it. If they have the capacity, they should assign themselves to this issue.
1. [ ] QA of your Sketch file by the reviewer.
1. [ ] Add your changes to the [pattern library](/gitlab-pattern-library.sketch). When copying things over, watch out for duplicated symbols, layer styles, and text styles. Some of our [recommended plugins](/CONTRIBUTING.md#plugins) can help you find and remove duplicates.
1. [ ] QA of pattern library by the reviewer.
1. [ ] [Create an issue in the Design System](https://gitlab.com/gitlab-org/design.gitlab.com/issues/new) to add the pattern documentation. Mark it as related to this issue.
1. [ ] Add an agenda item to the next [UX weekly call](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/edit?usp=sharing) to inform everyone (if it's a new pattern, not yet used in the application).

/label ~"UX" ~"pattern library" ~Pajamas
/cc @gitlab-com/gitlab-ux
